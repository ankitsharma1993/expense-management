class AddReferenceIdsToTables < ActiveRecord::Migration
  def change
    add_column :transactions, :account_id, :integer
    add_column :accounts, :user_id, :integer
  end
end
