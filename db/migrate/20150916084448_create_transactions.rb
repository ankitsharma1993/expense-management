class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.string :from_accountName
      t.string :to_accountName
      t.integer :amount
      t.string :description

      t.timestamps null: false
    end
  end
end
