class HomeController < ApplicationController

  before_action :authenticate_user!

  def index
    @transaction = Transaction.new
  end

  def create_transaction
    if transaction_params["t_type"]=="debit"
      hash = { "amount"=> -transaction_params["amount"].to_i }
    else
      hash = {}
    end
    trans_params = transaction_params.update(hash)
    respond_to do |format|
      if current_user.transactions.create(trans_params)
        validate = update_account(trans_params)
        if validate!=0
          format.js {}
        else
          Transaction.last.delete
          format.js {render 'error'}
        end
      end
    end
  end

  def add_account
    respond_to do |format|
      if current_user.accounts.create(account_params)
        format.js {}
      end
    end
  end

  def add_tag
    respond_to do |format|
      if Hashtag.create(hashtag_params)
        format.js {}
      end
    end
  end

  def delete_transaction
    transaction = Transaction.find(params[:t_id])
    amount = transaction.amount
    type = transaction.t_type
    if type == "debit"
      from_account = transaction.from_accountName
      account = Account.find_by_name(from_account)
      amt = account.amount - amount
      account.update(amount: amt )
    elsif type == "credit"
      to_account = transaction.to_accountName
      account = Account.find_by_name(to_account)
      amt = account.amount - amount
      account.update(amount: amt )
    elsif type == "tranfer"
      to_accountname = transaction.to_accountName
      to_account = Account.find_by_name(to_accountname)
      from_accountname = transaction.from_accountName
      from_account = Account.find_by_name(from_accountname)
      from_amt = from_account.amount + amount
      to_amt = to_account.amount - amount
      from_account.update(amount: from_amt )
      to_account.update(amount: to_amt )
    end
    respond_to do |format|
      if Transaction.find(params[:t_id]).delete
        format.js {render 'create_transaction'}
      else
        format.js {render 'error'}
      end
    end
  end

  def filter
    filter = params[:filter]
    name = params[:name]
    respond_to do |format|
      if filter == "account"
        @transactions = current_user.transactions.where("from_accountName =? OR to_accountName = ?", name, name)
      elsif filter == "tag"
        @transactions = current_user.transactions.where(tag: name)
      elsif filter == "type"
        names = name.split(',')
        @transactions = current_user.transactions.where(t_type: names)
      end
      format.js {render 'filter', object: @transactions}
    end
  end

  private

  def account_params
    params.permit(:name, :amount)
  end

  def hashtag_params
    params.permit(:tag)
  end

  def transaction_params
    params["transaction"].permit(:t_type, :tag, :amount, :description, :from_accountName, :to_accountName);
  end

  def update_account(params)
    if (params["t_type"]=="debit")
      account = Account.find_by_name(params["from_accountName"])
      amount = account.amount + params["amount"]
      if (amount < 0)
        return 0
      else
        account.update("amount"=>amount)
      end
    elsif (params["t_type"]=="credit")
      account = Account.find_by_name(params["to_accountName"])
      amount = account.amount + params["amount"].to_i
      account.update("amount"=>amount)
    else
      from_account = Account.find_by_name(params["from_accountName"])
      to_account = Account.find_by_name(params["to_accountName"])
      if (from_amount >= 0)
        from_amount = from_account.amount + params["amount"].to_i
        to_amount = to_account.amount + params["amount"].to_i
        from_account.update("amount"=>amount)
        to_account.update("amount"=>amount)
      else
        return 0;
      end
    end
  end
end
